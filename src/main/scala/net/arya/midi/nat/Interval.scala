package net.arya.midi.nat

/**
 * Created by arya on 7/4/14.
 */
import shapeless.Nat
import shapeless.nat._
import shapeless.ops.nat.LTEq.<=
import shapeless.ops.nat.{Sum, Diff, ToInt}
import scala.annotation.implicitNotFound

case class Interval[D<:Nat:ToInt, S<:Nat:ToInt]() {
  def invert[DIO<:Nat, DSO<:Nat](implicit e1: D <= _8, e2: S <= _12,
                                 dD: Diff.Aux[_9,D,DIO], dS: Diff.Aux[_12,S,DSO],
                                 e7: ToInt[DIO], e8: ToInt[DSO]) =
    new Interval[dD.Out, dS.Out]

  def compound[D2<:Nat,S2<:Nat,DO<:Nat,SO<:Nat,DDO<:Nat](that: Interval[D2,S2])
                                                        (implicit DD: Diff.Aux[D2,_1,DDO],
                                                         DS: Sum.Aux[D,DDO,DO],
                                                         SS: Sum.Aux[S,S2,SO],
                                                         DOI: ToInt[DO], SOI: ToInt[SO]
                                                          ) = Interval[DO,SO]

  override def toString = s"[${ToInt[D].apply},${ToInt[S].apply}]"
}

case class IntervalNoStr[D<:Nat, S<:Nat]() {
  def invert(implicit e1: D <= _8, e2: S <= _12,
             dD: Diff[_9,D], dS: Diff[_12,S]) = new IntervalNoStr[dD.Out, dS.Out]

  def compound[D2<:Nat,S2<:Nat,DDO<:Nat](that: IntervalNoStr[D2,S2])
                               (implicit DD: Diff.Aux[D2,_1,DDO], DS: Sum[D,DDO], SS: Sum[S,S2]
                                 ) = IntervalNoStr[DS.Out,SS.Out]

}


object Interval {
  type unison = Interval[_1,_0]
  type augUnison = Interval[_1,_1]
  type min2nd = Interval[_2,_1]
  type maj2nd = Interval[_2,_2]
  type aug2nd = Interval[_2,_3]
  type min3rd = Interval[_3,_3]
  type maj3rd = Interval[_3,_4]
  type dim4th = Interval[_4,_4]
  type perf4th = Interval[_4,_5]
  type aug4th = Interval[_4,_6]
  type dim5th = Interval[_5,_6]
  type perf5th = Interval[_5,_7]
  type aug5th = Interval[_5,_8]
  type min6th = Interval[_6,_8]
  type maj6th = Interval[_6,_9]
  type dim7th = Interval[_7,_9]
  type min7th = Interval[_7,_10]
  type maj7th = Interval[_7,_11]
  type dimOctave = Interval[_8,_11]
  type octave = Interval[_8,_12]

  val unison = Interval[_1,_0]
  val augUnison = Interval[_1,_1]
  val min2nd = Interval[_2,_1]
  val maj2nd = Interval[_2,_2]
  val aug2nd = Interval[_2,_3]
  val min3rd = Interval[_3,_3]
  val maj3rd = Interval[_3,_4]
  val dim4th = Interval[_4,_4]
  val perf4th = Interval[_4,_5]
  val aug4th = Interval[_4,_6]
  val dim5th = Interval[_5,_6]
  val perf5th = Interval[_5,_7]
  val aug5th = Interval[_5,_8]
  val min6th = Interval[_6,_8]
  val maj6th = Interval[_6,_9]
  val dim7th = Interval[_7,_9]
  val min7th = Interval[_7,_10]
  val maj7th = Interval[_7,_11]
  val dimOctave = Interval[_8,_11]
  val octave = Interval[_8,_12]

  val min9th = octave compound min2nd
  val maj9th = octave compound maj2nd
}
