package net.arya.midi

import javax.sound.midi.{MidiMessage, Receiver, MidiSystem}

import com.googlecode.scala.sound.midi.message.{NoteOff, NoteOn}

/**
 * Created by arya on 7/5/14.
 */
object Player {
//  val timeStamp = midiDevice.getMicrosecondPosition
  val rcvr = MidiSystem.getReceiver


  def playNote(n: StandardPitchNote, time: Int) {
    rcvr.send(NoteOn(0, n.midiNote, 93), -1)
    Thread.sleep(time)
    rcvr.send(NoteOff(0, n.midiNote, 93), -1)
  }

  def playAscendingDescendingScale(m: DiatonicMode, root: StandardPitchNote) {
    val scale = DiatonicScale.scaleForMode(m)(Note(root.nn,root.acc))
    val ascending = Scale.ascending(scale,root.octave)
    playSequence(ascending ++ ascending.reverse, 200)
  }

  def playChord(notes: Seq[StandardPitchNote], millis: Int = 1000) {
    notes.foreach(n => rcvr.send(NoteOn(0, n.midiNote, 93), -1))
    Thread.sleep(millis)
    notes.foreach(n => rcvr.send(NoteOff(0, n.midiNote, 93), -1))
  }

  def playSequence(notes: Seq[StandardPitchNote], millis: Int = 200) {
    notes.foreach(playNote(_, millis))
  }

  implicit class playerSyntax(notes: Seq[StandardPitchNote]) {
    def playChord: Unit = playChord()
    def play: Unit = play()
    def playChord(millis: Int = 1000): Unit = Player.playChord(notes, millis)
    def play(millis: Int = 200): Unit = Player.playSequence(notes, millis)
  }
}
