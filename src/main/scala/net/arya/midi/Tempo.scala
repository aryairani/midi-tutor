package net.arya.midi

/**
 * Created by arya on 7/4/14.
 */
case class Tempo(bpm: Double) extends AnyVal {
  def msPerBeat = 60*1000/bpm
}

//case class TimeSignature(upper: Int, lower: Int) {
//
//}
