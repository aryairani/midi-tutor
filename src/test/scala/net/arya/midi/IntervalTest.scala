package net.arya.midi

import org.scalatest.FunSuite
import Interval._

/**
 * Created by arya on 7/6/14.
 */
class IntervalTest extends FunSuite {
  test("adding intervals") {
    assert(unison + unison == unison)
    assert(unison + augUnison != unison)
    assert(unison + augUnison == augUnison)
    assert(perf4th + perf5th == octave)
    assert(maj2nd + octave == maj9th)

  }
}
