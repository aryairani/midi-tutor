package net.arya.midi

import net.arya.midi._
import util.Invert

/**
 * Created by arya on 7/4/14.
 */
package object syntax {

  implicit class ichord(c: IChord) {
    def from(root: Note): NChord = Scale.apply(c,root)
//    def invert(count: Int = 1): IChord = Invert(c, count)
    def invert(i: Inversion): IChord = Invert(c, i.count)
    def apply(i: Inversion): IChord = Invert(c, i.count)

    // todo check re compoundintervals
    def rootedAt(i: Interval): IChord = Invert.toRootOrAdd(i, c)
    def /(i: Interval) = rootedAt(i)
  }

  implicit class nchord(c: NChord) {
    def rootedAt(n: Note): NChord = Invert.toRootOrAdd(n, c)
    def /(n: Note) = rootedAt(n)
    def apply(i: Inversion): NChord = Invert(c, i.count)
  }

  implicit class noteseq(notes: Seq[Note]) {
    def ascending: Seq[RPN] = Note.ascending(notes)
    def descending: Seq[RPN] = Note.descending(notes)
    def ascending(startOctave: Octave): Seq[SPN] = ascending.octave(startOctave)
    def descending(startOctave: Octave): Seq[SPN] = descending.octave(startOctave)
  }

  implicit class rpnseq(s: Seq[RPN]) {
    def octave(o: Int) = s.map(_.octave(Octave(o)))
    def octave(o: Octave) = s.map(_.octave(o))
  }
}
