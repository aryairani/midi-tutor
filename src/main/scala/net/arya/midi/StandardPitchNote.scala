package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */
case class StandardPitchNote(n: Note, octave: Octave) {
  def o = octave.o
  def nn = n.nn
  def acc = n.a
  override def toString = if (acc == Accidental.natural) s"$nn$o " else s"$nn$acc$o"
  def pitchClass = Note(nn,acc).pitchClass
  def midiNote: Int = pitchClass.pc.int + (o+1) * 12
}

object StandardPitchNote {
  def apply(n: Note, o: Int): SPN = SPN(n, Octave(o))
  def apply(nn: NaturalNote, acc: Accidental, o: Int): SPN = SPN(Note(nn, acc), o)
  def apply(nn: NaturalNote, acc: Accidental, octave: Octave): SPN = SPN(Note(nn, acc), octave.o)

  def fromMidiNote(n: Int): StandardPitchNote = {
    val o = ((n-12)/12.0).floor.toInt // ugh
    PitchClass(n).toNoteSharp.octave(o)
  }
}


case class RelativePitchNote(n: Note, relOctave: RelOctave) {
  def nn = n.nn
  def acc = n.a
  def ro = relOctave.o
  override def toString = s"$nn$acc[$ro]"
  def octave(o: Octave) = StandardPitchNote(n, o+relOctave)
}