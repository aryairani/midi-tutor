package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */
class StandardPitchNoteTest extends org.scalatest.FunSuite {
  import NaturalNote._
  import StandardPitchNote.fromMidiNote

  test("Midi notes should match the right octaves") {
    assert(fromMidiNote(60) == (C ♮ 4))
    assert(fromMidiNote(12) == (C ♮ 0))
    assert(fromMidiNote(11) == (B ♮ -1))
    assert(fromMidiNote(0) == (C ♮ -1))
  }
}
