package net.arya.midi

import language.postfixOps
import net.arya.midi.NaturalNote._

/**
 * Created by arya on 7/3/14.
 */


sealed trait KeySig
case object Natural extends KeySig
case class Sharps(s: Int) extends KeySig
case class Flats(f: Int) extends KeySig

object Key {
  def forMajorPitchClass: PitchClass => KeySig =
    KeySig.forMajorKey compose
      List(D♭,A♭,E♭,B♭,F♮,C♮,G♮,D♮,A♮,E♮,B♮,F♯)
        .map(k => k.pitchClass -> k)
        .toMap
}

object KeySig {
  val sharps = List(F,C,G,D,A,E,B)
  val flats = List(B,E,A,D,G,C,F)

  def forMajorKey: Note => KeySig = Map(
    (C♭, Flats(7)),
    (G♭, Flats(6)),
    (D♭, Flats(5)),
    (A♭, Flats(4)),
    (E♭, Flats(3)),
    (B♭, Flats(2)),
    (F♮, Flats(1)),
    (C♮, Natural),
    (G♮, Sharps(1)),
    (D♮, Sharps(2)),
    (A♮, Sharps(3)),
    (E♮, Sharps(4)),
    (B♮, Sharps(5)),
    (F♯, Sharps(6)),
    (C♯, Sharps(7))
  )

  def forMinorKey: Note => KeySig =
    forMajorKey compose Interval.min3rd.from
  
  implicit class keySyntax(k: KeySig) {
    def addSharp = k match {
      case Flats(n) => if (n == 1) Natural else Flats(n-1)
      case Natural => Sharps(1)
      case Sharps(n) => Sharps(n+1)
    }
    def addFlat = k match {
      case Sharps(n) => if (n == 1) Natural else Sharps(n-1)
      case Natural => Flats(1)
      case Flats(n) => Flats(n+1)
    }
  }
}