package net.arya.midi

/**
 * Created by arya on 7/7/14.
 */
case class Octave(o: Int) {
  def +(rel: RelOctave) = Octave(o + rel.o)
}

object Octave {
  def raise = (o:Octave) => Octave(o.o+1)
  def lower = (o:Octave) => Octave(o.o-1)
}

case class RelOctave(o: Int) {
  def +(rel: RelOctave) = RelOctave(o + rel.o)
  def +(o: Octave) = o + this
}

object RelOctave {
  def raise = (r:RelOctave) => RelOctave(r.o+1)
  def lower = (r:RelOctave) => RelOctave(r.o-1)
}