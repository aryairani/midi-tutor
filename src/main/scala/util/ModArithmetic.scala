package util

/**
 * Created by arya on 7/3/14.
 */
case class ModArithmetic(n: Int) {
  case class ModInt(int: Int) // i < n
  require(n > 0)

  val elements = 0 to n

  def assertPositive(xs: Int*) {
    xs foreach { x =>
      require(x >= 0)
    }
  }

  def assertRange(xs: Int*) {
    xs foreach { x =>
      require(x >= 0)
      require(x < n)
    }
  }

  def mod(a: Int): ModInt = {
    if (a > 0)
      ModInt(a % n)
    else mod(a + n)
  }

  def plus(a: ModInt, b: Int): ModInt =
    plus(a, mod(b))

  def plus(a: Int, b: Int): ModInt = {
    assertPositive(a,b)
    ModInt { (a+b) % n }
  }

  def plus(a: ModInt, b: ModInt): ModInt = {
    ModInt { (a.int + b.int) % n }
  }

  def minus(a: ModInt, b: ModInt): ModInt = {
    ModInt { (a.int - b.int + n) % n }
  }
}
