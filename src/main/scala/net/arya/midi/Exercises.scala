package net.arya.midi

import net.arya.midi.NaturalNote._
import Interval._
import util.ModArithmetic

/**
 * Created by arya on 7/5/14.
 */
object Exercise {
  val octave = 4
  val startNote = C♮octave

  val fifths = Seq.iterate(startNote,13) {
    case StandardPitchNote(n, oct) =>
      StandardPitchNote(Interval.perf5th.from(n), octave)
  }

  import IChord._
  val chords = Seq(maj7, min7, dom7, halfdim7, dim7, sus7)

//  for {
//    chord <- chords
//    root <- fifths
//  } Player.playChord(Scale.ascending(chord.components.map(_ from root.n), 4), 500)

  import Degree._
  import Inversions._
  import syntax._
  val chordProgression: Seq[IDegreeChord] =
    Seq(ii(min7), V(dom7) invi 2, I(maj7), vi(min7) invi 1)

  val chordProgression4: Seq[IDegreeChord] = for {
    i <- 0 to 3
    c <- chordProgression
  } yield c invi ModArithmetic(4).mod(4-i).int
}
