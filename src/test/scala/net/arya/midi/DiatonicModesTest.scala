package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */
class DiatonicModesTest extends org.scalatest.FunSuite {
  import DiatonicMode._
  import NaturalNote._
  import language.postfixOps

  test("Diatonic mode scales") {
    assert(DiatonicScale(C♮, dorian) == Seq(C♮,D♮,E♭,F♮,G♮,A♮,B♭,C♮))
  }
}
