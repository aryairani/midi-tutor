package net.arya.midi

import javax.sound.midi.{MidiUnavailableException, MidiMessage, Receiver, MidiSystem}

import com.googlecode.scala.sound.midi.MidiEvent
import com.googlecode.scala.sound.midi.message.NoteOn

/**
 * Created by arya on 7/12/14.
 */
object Listener {
  val devInfo = MidiSystem.getMidiDeviceInfo
  val xmit = try {
    MidiSystem.getTransmitter
  } catch {
    case ex: MidiUnavailableException =>
      println("Couldn't lookup a MIDI source.  Try restarting the JVM?")
      sys.exit()
  }

  var activeSensing = false

  private val printReceiver = new Receiver {
    override def send(message: MidiMessage, timeStamp: Long): Unit = message match {
      case NoteOn(chan, key, vel) =>
        if (vel > 0)
          println(s"${SPN.fromMidiNote(key)} @ $timeStamp")

      case ActiveSensing() =>
        if (!activeSensing) {
          activeSensing = true
          println("+Active Sensing")
        }

      case other => println(s"other message: " +
        message.getMessage.map(_.formatted("%02X")).mkString(" "))

    }

    override def close(): Unit = {}
  }

  xmit.setReceiver(printReceiver)
}

object ActiveSensing {
  def unapply(msg: javax.sound.midi.ShortMessage) = {
    if (msg.getStatus == javax.sound.midi.ShortMessage.ACTIVE_SENSING) {
      Some()
    } else None
  }
}

object Test extends App {
  Listener
}
