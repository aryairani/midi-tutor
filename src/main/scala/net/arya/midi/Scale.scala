package net.arya.midi

import net.arya.midi.Interval._
import util.Invert
import syntax._

/**
 * Created by arya on 7/3/14.
 */
object Scale {
  def apply(scale: Seq[Interval], root: Note): NScale =
    Seq.tabulate(scale.length)(idx => scale(idx).from(root))

  def iterate(increments: Seq[Interval], root: Note): NScale =
    Seq.iterate[(Note,Int)]((root,0),increments.length+1){
      case (n,idx) => (increments(idx).from(n), idx+1)
    }.unzip._1

  def descending(s: NScale, startOctave: Octave): PScale =
    s.descending(startOctave)

  def ascending(s: NScale, startOctave: Octave): PScale =
    s.ascending(startOctave)
}

case class DiatonicMode(fromMajorTonic: Int) extends AnyVal
object DiatonicMode {
  val ionian = DiatonicMode(1)
  val dorian = DiatonicMode(2)
  val phrygian = DiatonicMode(3)
  val lydian = DiatonicMode(4)
  val mixolydian = DiatonicMode(5)
  val aeolian = DiatonicMode(6)
  val locrian = DiatonicMode(7)
}

object DiatonicScale {
  val T = maj2nd // tone
  val S = min2nd // semitone
  val major = DiatonicMode.ionian
  val minor = DiatonicMode.aeolian
  val majorAbsolute = Vector(unison, maj2nd, maj3rd, perf4th, perf5th, maj6th, maj7th)
  val majorIncremental = Vector(T,T,S,T,T,T,S)
  val minorIncremental = Invert(majorIncremental, 6)
  def scaleForMode(mode: DiatonicMode): Note => NScale =
    apply(_, mode)
  
  def apply(root: Note, mode: DiatonicMode = major): NScale = {
    val (start,end) = majorIncremental.splitAt(mode.fromMajorTonic - 1)
    Scale.iterate(end ++ start, root)
  }
}
