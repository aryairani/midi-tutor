package net.arya.midi

import scala.annotation.tailrec

/**
 * Created by arya on 7/3/14.
 */
trait Interval {
  val diatonic, semitones: Int

  def +(that: Interval): Interval = {
    if (this.diatonic + that.diatonic > 9)
      CompoundInterval(this.diatonic + that.diatonic - 1, this.semitones + that.semitones)
    else SmallInterval(this.diatonic + that.diatonic - 1, this.semitones + that.semitones)
  }

  val from: Note => Note = {
    case root @ Note(nn, a) => {
      val newNN = DiatonicInterval(diatonic).from(nn)
      val targetPitchClass = root.pitchClass.addSemitones(semitones)
      val accidental = Accidental.find(newNN, targetPitchClass)
      Note(newNN, accidental)
    }
  }
}

case class SmallInterval(diatonic: Int, semitones: Int) extends Interval {
  require(1 to 8 contains diatonic)
  require(0 to 12 contains semitones)

  def invert = SmallInterval(9 - diatonic, 12 - semitones)
}

case class CompoundInterval(diatonic: Int, semitones: Int) extends Interval

case class DiatonicInterval(width: Int) {
  require(width >= 1)

  @tailrec final def from(nn: NaturalNote): NaturalNote =
    if (width == 1) nn else DiatonicInterval(width-1).from(nn.next)
}
object Interval {
  // change to enumeration? e.g. http://downgra.de/2010/02/11/playing-with-scala-enumeration/

  val unison = SmallInterval(1,0)
  val augUnison = SmallInterval(1,1)
  val min2nd = SmallInterval(2,1)
  val maj2nd = SmallInterval(2,2)
  val aug2nd = SmallInterval(2,3)
  val min3rd = SmallInterval(3,3)
  val maj3rd = SmallInterval(3,4)
  val dim4th = SmallInterval(4,4)
  val perf4th = SmallInterval(4,5)
  val aug4th = SmallInterval(4,6)
  val dim5th = SmallInterval(5,6)
  val perf5th = SmallInterval(5,7)
  val aug5th = SmallInterval(5,8)
  val min6th = SmallInterval(6,8)
  val maj6th = SmallInterval(6,9)
  val dim7th = SmallInterval(7,9)
  val min7th = SmallInterval(7,10)
  val maj7th = SmallInterval(7,11)
  val dimOctave = SmallInterval(8,11)
  val octave = SmallInterval(8,12)

  val diatonicSemitone = min2nd
  val diatonicTone = maj2nd
  val chromaticSemitone = augUnison

  // see http://www.tonalsoft.com/enc/encyclopedia-index.aspx generic diatonic interval names
  val dim9th = CompoundInterval(9,12)
  val min9th = CompoundInterval(9,13)
  val maj9th = CompoundInterval(9,14)
  val aug9th = CompoundInterval(9,15)
}

// i + x - 1 == 8
// x == 8+1 - i

