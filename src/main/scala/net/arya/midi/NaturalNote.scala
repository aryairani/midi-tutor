package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */
object NaturalNote {
  val set = Set[NaturalNote](A,B,C,D,E,F,G)
  case object A extends NaturalNote
  case object B extends NaturalNote
  case object C extends NaturalNote
  case object D extends NaturalNote
  case object E extends NaturalNote
  case object F extends NaturalNote
  case object G extends NaturalNote

  def next: NaturalNote => NaturalNote = {
    case A => B
    case B => C
    case C => D
    case D => E
    case E => F
    case F => G
    case G => A
  }
  def prev: NaturalNote => NaturalNote = {
    case A => G
    case B => A
    case C => B
    case D => C
    case E => D
    case F => E
    case G => F
  }
}

/** A B C D E F G */
sealed trait NaturalNote {
  def next = NaturalNote.next(this)
  def prev = NaturalNote.prev(this)

  /* accidentals */
  private val acc = Accidental
  private val n = StandardPitchNote
  private type N = StandardPitchNote

  def ♮ : Note = Note(this, acc.♮)
  def ♯ : Note = Note(this, acc.♯)
  def ♯♯ : Note = Note(this, acc.♯♯)
  def ♭ : Note = Note(this, acc.♭)
  def ♭♭ : Note = Note(this, acc.♭♭)
  def natural: Note = Note(this, acc.♮)
  def sharp: Note = Note(this, acc.♯)
  def doubleSharp: Note = Note(this, acc.♯♯)
  def flat: Note = Note(this, acc.♭)
  def doubleFlat: Note = Note(this, acc.♭♭)

  /* to notes */
  def ♮(o: Integer): N = natural(o)
  def ♯(o: Integer): N = sharp(o)
  def ♯♯(o: Integer): N = doubleSharp(o)
  def ♭(o: Integer): N = flat(o)
  def ♭♭(o: Integer): N = doubleFlat(o)
  def natural(o: Integer): N  = n(natural, o)
  def sharp(o: Integer): N = n(sharp, o)
  def doubleSharp(o: Integer): N = n(doubleSharp, o)
  def flat(o: Integer): N = n(flat, o)
  def doubleFlat(o: Integer): N = n(doubleFlat, o)
  
  /* to chords */
//  def ♮(c: Chord): Seq[Note] = c from ♮
//  def natural(c: Chord): Seq[Note]  = c from ♮
//  def ♯(c: Chord): Seq[Note] = c from ♯
//  def sharp(c: Chord): Seq[Note] = c from ♯
//  def ♯♯(c: Chord): Seq[Note] = c from ♯♯
//  def doubleSharp(c: Chord): Seq[Note] = c from ♯♯
//  def ♭(c: Chord): Seq[Note] = c from ♭
//  def flat(c: Chord): Seq[Note] = c from ♭♭
//  def ♭♭(c: Chord): Seq[Note] = c from ♭♭
//  def doubleFlat(c: Chord): Seq[Note] = c from ♭♭
  
}