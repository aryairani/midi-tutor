package net.arya.midi


/**
 * Created by arya on 7/6/14.
 */
object Scratch {
  import net.arya.midi._
  import NaturalNote._
  import syntax._
  import language.postfixOps
  val chords = Exercise.chordProgression4.map(_.fromTonic(A flat))
  chords.foreach(c => println(c.mkString(" ")))
  /*
B♭ D♭ F A♭
...
   */

  // play
  chords.map(_.ascending.octave(4)).foreach(Player.playChord(_, 400))

  // play circle of 4ths
  import Player.playerSyntax
  (0 to 12).toList.foldLeft(C sharp) {
    case (k,_) =>
      val scale = DiatonicScale(k).ascending
      println(scale.map(_.n).mkString(" "))
      scale.octave(3).play
      Interval.perf4th.from(k)
  }

}
