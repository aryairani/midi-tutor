package net.arya

/**
 * Created by arya on 7/3/14.
 */
package object midi {

  type IChord = Seq[Interval]
  type NChord = Seq[Note]
  type PChord = Seq[StandardPitchNote]

  type IScale = Seq[Interval]
  type NScale = Seq[Note]
  type PScale = Seq[StandardPitchNote]

  type RPN = RelativePitchNote
  type SPN = StandardPitchNote
  val RPN = RelativePitchNote
  val SPN = StandardPitchNote

}
