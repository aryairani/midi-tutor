package net.arya.midi

/**
 * Created by arya on 7/6/14.
 */
sealed trait Direction
case object Ascending extends Direction
case object Descending extends Direction