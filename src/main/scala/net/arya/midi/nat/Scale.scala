package net.arya.midi.nat

import net.arya.midi.Note
import shapeless.ops.nat.LT.<
import shapeless.ops.nat.{Sum, ToInt, Diff, Pred}
import shapeless.{Succ, Sized, Nat}
import shapeless.nat._

import scala.collection.generic.IsTraversableLike

/**
 * Created by arya on 7/5/14.
 */

case class DiatonicMode[FromMajorTonic<:Nat]() {
  type N = FromMajorTonic
}
object DiatonicMode {
  type Ionian = DiatonicMode[_1]
  type Dorian = DiatonicMode[_2]
  type Phrygian = DiatonicMode[_3]
  type Lydian = DiatonicMode[_4]
  type Mixolydian = DiatonicMode[_5]
  type Aeolian = DiatonicMode[_6]
  type Locrian = DiatonicMode[_7]

  val Ionian = DiatonicMode[_1]
  val Dorian = DiatonicMode[_2]
  val Phrygian = DiatonicMode[_3]
  val Lydian = DiatonicMode[_4]
  val Mixolydian = DiatonicMode[_5]
  val Aeolian = DiatonicMode[_6]
  val Locrian = DiatonicMode[_7]

}
object DiatonicScale {
  import Interval._
  type T = maj2nd
  type S = min2nd
  val T = maj2nd
  val S = min2nd
  val diatonicMajorIntervalsTS = Sized(T,T,S,T,T,T,S) // _7

//  // N == M-1
//  def scaleForMode[N<:Nat,D](mode: DiatonicMode[Succ[N]], root: Note)(implicit diff: Diff.Aux[_7,N,D], sum: Sum[D,N], n: ToInt[N]): Seq[Note] = {
//    val (start,end) = diatonicMajorIntervalsTS.splitAt[N]
//    Scale.iterate(end ++ start, root)
//  }
}
//
//object Scale {
//  // IntervalCount (7) + Accum (0) + 1 = Total(8)
//  // IntervalCount (7) + Accum (0) = TotalMinus1(7)
//  // return Succ[TotalMinus1] = 7+1 = 8
//  type Pos[N<:Nat] = _0 < N
//  def iterate[IC<:Nat, A<:Nat:Pos, R1, R2[_], T_1<:Nat](increments: Sized[R1,IC],
//                                                            acc: Sized[R2[Note],A])
//                                                           (implicit //sum: Sum.Aux[IC,A,T_1],
//                                                            itl:IsTraversableLike[R2[Note]]
//                                                             ) =
//  {
//
//  }
//
//  def iterate[ScaleLength<:Nat,R](increments: Sized[R,ScaleLength], root: Note) = ???
//}


