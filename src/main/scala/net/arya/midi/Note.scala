package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */

import net.arya.midi.NaturalNote._
import language.postfixOps
import scala.annotation.tailrec
import net.arya.midi.Interval._


/* C♯ etc */
case class Note(nn: NaturalNote, a: Accidental) {
  def raiseAccidental = this.copy(a = a.raise)
  def lowerAccidental = this.copy(a = a.lower)
  def pitchClass: PitchClass = {
    PitchClass {
      (nn match {
        case C => 0
        case D => 2
        case E => 4
        case F => 5
        case G => 7
        case A => 9
        case B => 11
      }) + a.semitoneOffset
    }
  }


  // todo: this may result in the wrong octave in some cases
  // find an equivalent note with smaller accidental
  def findNaturalNote: Option[Note] =
    NaturalNote.set.find(n => n.natural.pitchClass == this.pitchClass).map(_.natural)

  def normalize: Note =
    findNaturalNote.getOrElse {
      if (a.semitoneOffset > 1)
        this.lowerAccidental.findNaturalNote.map(_.raiseAccidental).get
      else if (a.semitoneOffset < 1)
        this.raiseAccidental.findNaturalNote.map(_.lowerAccidental).get
      else this
    }

  //  pick a same-sounding key with fewest accidentals
  import syntax.ichord

  def apply(o: Int) = octave(o)
  def apply(c: IChord) = c.from(this)
  def octave(o: Int) = StandardPitchNote(nn, a, o)
  override def toString = if (a == Accidental.♮) s"$nn " else s"$nn$a"
}

object Note {

  def monotony(notes: Seq[Note],
               cmp: (PitchClass,PitchClass) => Boolean,
               modOctave: RelOctave => RelOctave): Seq[RPN] =

    notes.foldLeft(List.empty[RPN]) {
      case (Nil, n) => RPN(n, RelOctave(0)) :: Nil
      case (last :: rest, n) =>
        val newRelOctave =
          if (cmp(last.n.pitchClass, n.pitchClass))
            modOctave(last.relOctave)
          else last.relOctave
        RPN(n, newRelOctave) :: last :: rest
    }.reverse

  def ascending(notes: Seq[Note]): Seq[RPN] =
    monotony(notes, implicitly[Ordering[PitchClass]].gteq, RelOctave.raise)

  def descending(notes: Seq[Note]): Seq[RPN] =
    monotony(notes, implicitly[Ordering[PitchClass]].lteq, RelOctave.lower)


}