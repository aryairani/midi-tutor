package net.arya.midi

/**
 * Created by arya on 7/3/14.
 */

/** ♭ ♮ ♯ etc */
case class Accidental(semitoneOffset: Int) extends AnyVal {
  def raise = Accidental(semitoneOffset + 1)
  def lower = Accidental(semitoneOffset - 1)
  override def toString = {
    import Accidental._
    this match {
      case `♭♭` => "𝄫"
      case `♭` => "♭"
      case `♮` => "♮"
      case `♯` => "♯"
      case `♯♯` => "𝄪"
    }
  }
}
object Accidental {
  val ♭♭, `𝄫`, doubleFlat = Accidental(-2)
  val ♭, b, flat = Accidental(-1)
  val ♮, natural = Accidental(0)
  val ♯, sharp = Accidental(1)
  val ♯♯, `𝄪`, doubleSharp = Accidental(2)

  val known = List(♮,♭,♯,♭♭,♯♯)

  def find(reference: NaturalNote, target: PitchClass): Accidental = {
    Accidental.known.find(acc =>
      reference.natural.pitchClass.addSemitones(acc.semitoneOffset) == target
    ).getOrElse(throw new Exception(
      s"No accidental big enough to make a ${target.toNoteEasy} from a $reference!"
    ))
  }

}
