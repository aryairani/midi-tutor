package net.arya.midi

import language.postfixOps
import net.arya.midi.NaturalNote._
import util.ModArithmetic

/**
 * Created by arya on 7/3/14.
 */
case class PitchClass(pc: PitchClass.math.ModInt) extends Ordered[PitchClass] {
  import PitchClass.math._
  def addSemitones(s: Int) = PitchClass(plus(pc, s))
  def toNoteSharp: Note = pc.int match {
    case 0 => C♮
    case 1 => C♯
    case 2 => D♮
    case 3 => D♯
    case 4 => E♮
    case 5 => F♮
    case 6 => F♯
    case 7 => G♮
    case 8 => G♯
    case 9 => A♮
    case 10 => A♯
    case 11 => B♮
  }
  def toNoteFlat: Note = pc.int match {
    case 0 => C♮
    case 1 => D♭
    case 2 => D♮
    case 3 => E♭
    case 4 => E♮
    case 5 => F♮
    case 6 => G♭
    case 7 => G♮
    case 8 => A♭
    case 9 => A♮
    case 10 => B♭
    case 11 => B♮
  }
  def toNoteEasy: Note = pc.int match {
    case 0 => C♮
    case 1 => C♯
    case 2 => D♮
    case 3 => E♭
    case 4 => E♮
    case 5 => F♮
    case 6 => F♯
    case 7 => G♮
    case 8 => A♭
    case 9 => A♮
    case 10 => B♭
    case 11 => B♮
  }

  def compare(that: PitchClass): Int = this.pc.int - that.pc.int
}
object PitchClass {
  val math = ModArithmetic(12)
  def apply(int: Int): PitchClass = PitchClass(math.mod(int))

//  private val GT = (_:PitchClass) > (_:PitchClass)
//  private val lowerPC = (_:PitchClass) < (_:PitchClass)

}
