package net.arya.midi

/**
 * Created by arya on 7/6/14.
 */

case class IDegreeChord(d: Degree, c: IChord) {
  import syntax.ichord
  def apply(tonic: Note) = fromTonic(tonic)
  def apply(i: Inversion) = IDegreeChord(d,c invert i)
  def invert(i: Inversion) = IDegreeChord(d,c invert i)
  def invi(i: Int) = IDegreeChord(d,c invert Inversion(i))
  def fromTonic(tonic: Note): NChord = c.map(_ + d.pickFrom(DiatonicScale.majorAbsolute) from tonic)
  def rootedAt(i: Interval): IChord = ???
//  def /(i: Interval) = rootedAt(i)
  def run: IChord = rootedAt(Interval.unison)
}

case class Degree(d: Int) extends AnyVal {
  def apply(c: IChord) = IDegreeChord(this,c)
  def pickFrom(absoluteScale: IScale): Interval = absoluteScale(d-1)
}

object Degree {
  val I, tonic = Degree(1)
  val II, ii, supertonic = Degree(2)
  val III, iii, mediant = Degree(3)
  val IV, iv, subdominant = Degree(4)
  val V, v, dominant = Degree(5)
  val VI, vi, submediant = Degree(6)
  val VII, vii, subtonic = Degree(7)
}
