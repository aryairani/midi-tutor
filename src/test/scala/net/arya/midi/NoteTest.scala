package net.arya.midi

import net.arya.midi.NaturalNote._
import net.arya.midi.StandardPitchNote._
import org.scalatest.FunSuite

/**
 * Created by arya on 7/7/14.
 */
class NoteTest extends FunSuite {
  test(".normalize tests") {
    assert(C.♮.normalize == C.♮)

    assert(C.♯.normalize == C.♯)
    assert(C.♯♯.normalize == D.♮)

    assert(C.♭.normalize == B.♮)
    assert(C.♭♭.normalize == B.♭)

    assert(E.♯♯.normalize == F.♯)
    assert(E.♯.normalize == F.♮)
    assert(F.♭.normalize == E.♮)
    assert(F.♭♭.normalize == E.♭)
  }

}
