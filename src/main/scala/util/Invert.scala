package util

/**
 * Created by arya on 7/3/14.
 */

object Invert {
  def apply[A](components: Seq[A], count: Int): Seq[A] =
    if (count == 0) components
    else apply(components.tail :+ components.head, count - 1)

  def toRootOrAdd[A](value: A, seq: Seq[A]): Seq[A] = {
    val (before, equalAfter) = seq.span(_ != value)
    if (equalAfter.isEmpty) value +: seq // value is missing
    else equalAfter ++ before
  }
}
