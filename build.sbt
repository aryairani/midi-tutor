scalaVersion := "2.10.4"

libraryDependencies += "org.jfugue" % "jfugue" % "4.0.3" from "http://www.jfugue.org/jfugue-4.0.3.jar"

libraryDependencies += "com.googlecode.scala-midi" %% "scala-midi" % "0.2"

libraryDependencies += "com.chuusai" % "shapeless_2.10.4" % "2.0.0"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.0.6"

//libraryDependencies += "org.scalactic" % "scalactic" % "2.2.0" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.0" % "test"

resolvers ++= Seq(
  "Sonatype OSS Releases"  at "http://oss.sonatype.org/content/repositories/releases/",
  "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"
)

scalacOptions ++= Seq("-feature")