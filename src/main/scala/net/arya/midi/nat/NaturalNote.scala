package net.arya.midi.nat

import shapeless.Nat
import shapeless.nat._

/**
 * Created by arya on 7/4/14.
 */
trait NaturalNote {
  type PC<:Nat
}
object NaturalNote {
  type NN[_PC<:Nat] = NaturalNote { type PC = _PC }
  type _C = NN[_0]
  type _D = NN[_2]
  type _E = NN[_4]
  type _F = NN[_5]
  type _G = NN[_7]
  type _A = NN[_9]
  type _B = NN[_11]

//  case class Next[N<:NaturalNote]() extends NaturalNote
//  case class Prev[N<:NaturalNote]() extends NaturalNote

  case class Consecutive[N1<:NaturalNote,N2<:NaturalNote]()
  implicit val ab = Consecutive[_A,_B]
  implicit val bc = Consecutive[_B,_C]
  implicit val cd = Consecutive[_C,_D]
  implicit val de = Consecutive[_D,_E]
  implicit val ef = Consecutive[_E,_F]
  implicit val fg = Consecutive[_F,_G]
  implicit val ga = Consecutive[_G,_A]

//  case class Next[N<:NaturalNote]() extends NaturalNote
//  implicit def next[A<:NaturalNote,B<:NaturalNote](implicit c: Consecutive[A,B]): B =
}
//case class Succ[N<:NaturalNote]() extends NaturalNote
