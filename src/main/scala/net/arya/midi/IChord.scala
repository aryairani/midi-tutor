package net.arya.midi

import util.Invert

//case class PChord(components: StandardPitchNote*){
//  def invert(count: Int = 1) = PChord(Invert(components, count): _*)
//}
//
//
//case class NChord(components: Note*) {
//  def invert(count: Int = 1) = NChord(Invert(components, count): _*)
//}
//
//
//case class IChord(components: Interval*) {
//  def from(root: Note) = NChord(Scale.apply(components,root))
//  def invert(count: Int = 1) = IChord(Invert(components, count): _*)
//}



object IChord {
  def apply(components: Interval*) = components

  import Interval._
  val maj = IChord(unison, maj3rd, perf5th)
  val min = IChord(unison, min3rd, perf5th)
  val aug = IChord(unison, maj3rd, aug5th)
  val dim = IChord(unison, min3rd, dim5th)
  val maj6 = IChord(unison, maj3rd, perf5th, maj6th)
  val min6 = IChord(unison, min3rd, perf5th, min6th)
  val `7`, dom7 = IChord(unison, maj3rd, perf5th, min7th) // skill 2
  val Δ, maj7 = IChord(unison, maj3rd, perf5th, maj7th) // skill 1
  val m7, min7 = IChord(unison, min3rd, perf5th, min7th) // skill 3
  val aug7 = IChord(unison, maj3rd, aug5th, min7th)
  val o7,dim7 = IChord(unison, min3rd, dim5th, dim7th) // skill 5
  val ø,ø7,halfdim7 = IChord(unison, min3rd, dim5th, min7th) // skill 4
  val minmaj7 = IChord(unison, min3rd, perf5th, maj7th)
  val sus7 = IChord(unison, perf4th, perf5th, min7th) // skill 6

  // shell voicings
  val maj369 = IChord(maj3rd, maj6th, maj9th) // skill 7
  val min369 = IChord(min3rd, maj6th, maj9th) // skill 8

  val maj379 = IChord(maj3rd, maj7th, maj9th) // skill 9
  val dom379 = IChord(maj3rd, min7th, maj9th) // skill 10
  val min379 = IChord(min3rd, min7th, maj9th) // skill 11

  val maj735 = IChord(maj7th, maj3rd, perf5th) // skill 12
  val dom735 = IChord(min7th, maj3rd, perf5th) // skill 13
  val min735 = IChord(min7th, min3rd, perf5th) // skill 14
  val halfdim735 = IChord(min7th, min3rd, dim5th) // skill 15

  val maj736 = IChord(maj7th, maj3rd, maj6th) // skill 16    CΔ13
  val dom736 = IChord(min7th, maj3rd, maj6th) // skill 17    C13
  val min736 = IChord(min7th, min3rd, maj6th) // skill 18

  val dom479 = IChord(perf4th, min7th, maj9th) // skill 14  e.g. B♭/C
  val dom7946 = IChord(min7th, maj9th, perf4th, maj6th) // skill 15  e.g. B♭Δ/C

  // diatonic 7th chords
  import Degree._
  val diatonic7thChords = List(I(Δ), ii(m7), iii(m7), IV(Δ), V(`7`), vi(m7), vii(ø))

}


